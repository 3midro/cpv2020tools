package cpv2020tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;


public class EventosBotones extends JFrame {
    public EventosBotones() {
        super();
    }

    public String launchFileSearcher(JFrame frame) {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Elige la carpeta que contiene los DOCX");
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        chooser.showOpenDialog(frame);
        //File file = chooser.getCurrentDirectory();
        if (chooser.getSelectedFile()!=null){
                return chooser.getSelectedFile().toString().trim();
        }else{
                return "";
            }
    }

    public String beginProceso(String rutaOrigen, String rutaDestino) {
        String regresa = "";
        try {
            rutaOrigen = rutaOrigen.replace("\\", "\\\\");
            rutaDestino = rutaDestino.replace("\\", "\\\\");
            final File carpeta = new File(rutaOrigen);
            listarFicherosPorCarpeta(carpeta, rutaDestino);
            regresa = "LISTO";
            return regresa;
        } catch (Exception ex) {
            ex.printStackTrace();
            regresa = "ERROR";
            return regresa;
        }
    }

    public void listarFicherosPorCarpeta(final File carpeta, String rutaDestino) {
        for (final File ficheroEntrada : carpeta.listFiles()) {
            if (ficheroEntrada.isDirectory()) {
                listarFicherosPorCarpeta(ficheroEntrada, rutaDestino);
            } else {
                readFile(ficheroEntrada.getAbsolutePath(), ficheroEntrada.getName(), rutaDestino);
            }
        }
    }


    public void readFile(String archivo, String nomArch, String Path) {
        ArrayList<String> arList = new ArrayList();
        String[] linea = new String[7];
        String myLine = "";
        boolean flagHeader = false;
        int consecutivo = 0;
        try {
            FileInputStream fis = new FileInputStream(archivo);
            XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));
            List<XWPFParagraph> paragraphList = xdoc.getParagraphs();
            if (!flagHeader) {
                for (int p = 0; p < 6; p++) {
                    linea[p] = "|";
                }
                arList.add("N.P|NOMBRE|ORDEN EJECUCION|TEMA|SUBTEMA|DESCRIPCION|CUESTIONARIO");
                flagHeader = true;
            }
            for (XWPFParagraph paragraph : paragraphList) {
                if (paragraph.getText().contains("Nombre:")) {
                    linea[1] = paragraph.getText().replaceAll("Nombre:", "");
                }
                if (paragraph.getText().contains("Orden de ejecución:")) {
                    linea[2] = paragraph.getText().replaceAll("Orden de ejecución:", "");
                    ;
                }
                if (paragraph.getText().contains("Tema:")) {
                    linea[3] = paragraph.getText().replaceAll("Tema:", "");
                    ;
                }
                if (paragraph.getText().contains("Subtema:")) {
                    linea[4] = paragraph.getText().replaceAll("Subtema:", "");
                    ;
                }
                if (paragraph.getText().contains("Descripción:")) {
                    consecutivo++;
                    linea[0] = Integer.toString(consecutivo);
                    linea[5] = paragraph.getText().replaceAll("Descripción:", "");
                    linea[6] = nomArch.substring(((nomArch.length() - 8) * 1), (nomArch.length() - 5));
                    for (int p = 0; p <= 6; p++) {
                        myLine = myLine.concat(linea[p] + "|");
                    }
                    myLine = myLine.substring(0, myLine.length() - 1);
                    //System.out.println(myLine);
                    arList.add(myLine);
                    myLine = "";
                }
            }
            //CREAR XLSX

            //System.out.println(Arrays.deepToString(arList.toArray()));

            SXSSFWorkbook hwb = new SXSSFWorkbook();
            SXSSFSheet sheet = (SXSSFSheet) hwb.createSheet("DESCRIPCION_TEMAS");
            Integer constanteAnchoCeldas = 256;
            sheet.setColumnWidth(0, 11 * constanteAnchoCeldas);
            sheet.setColumnWidth(1, 16 * constanteAnchoCeldas);
            sheet.setColumnWidth(2, 19 * constanteAnchoCeldas);
            sheet.setColumnWidth(3, 57 * constanteAnchoCeldas);
            sheet.setColumnWidth(4, 57 * constanteAnchoCeldas);
            sheet.setColumnWidth(5, 68 * constanteAnchoCeldas);
            sheet.setColumnWidth(6, 15 * constanteAnchoCeldas);
            int rows = 0;

            XSSFCellStyle estilo = (XSSFCellStyle) hwb.createCellStyle();
            estilo.setBorderBottom(XSSFCellStyle.BORDER_NONE);
            estilo.setBorderLeft(XSSFCellStyle.BORDER_NONE);
            estilo.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
            estilo.setBorderTop(XSSFCellStyle.BORDER_NONE);
            estilo.setAlignment(XSSFCellStyle.ALIGN_CENTER);
            estilo.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
            estilo.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo.setLocked(true);
            estilo.setWrapText(true);
            estilo.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

            XSSFCellStyle encabezado = (XSSFCellStyle) hwb.createCellStyle();
            encabezado.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
            encabezado.setAlignment(XSSFCellStyle.ALIGN_CENTER);
            encabezado.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
            encabezado.setFillForegroundColor(HSSFColor.CORAL.index);
            encabezado.setLocked(true);
            encabezado.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            //System.out.println(arList.size());

            for (int k = 0; k < arList.size(); k++) {
                //myLine = arList.get(k).toString();
                myLine = arList.get(k);
                String data[] = myLine.split("\\|");
                SXSSFRow row = (SXSSFRow) sheet.createRow((short) 0 + rows);
                rows++;

                for (int p = 0; p < data.length; p++) {
                    SXSSFCell cell = (SXSSFCell) row.createCell((short) p);
                    cell.setCellValue(data[p]);
                    if (k == 0) {
                        cell.setCellStyle(encabezado);
                    } else {
                        cell.setCellStyle(estilo);
                    }

                }
            }

            nomArch = nomArch.substring(0, nomArch.length() - 5);
            String fileN = "Extracto_" + nomArch + ".xlsx";
            System.out.println(Path);
            FileOutputStream fileOut = new FileOutputStream(Path + "\\" + fileN);
            hwb.write(fileOut);
            fileOut.close();
            System.out.println("Your excel file has been generated");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
