package cpv2020tools;

import java.util.LinkedList;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFStyles;

public class GetComponents {
    public GetComponents() {
        super();
    }
    
    public List<String> getCriterios(List<XWPFParagraph> paragraphList, XWPFStyles styles){
        List<String> regresa = new LinkedList<>();
        for(int i = 0; i < paragraphList.size(); i++){
            if(paragraphList.get(i).getStyleID()!=null){
                if(styles.getStyle(paragraphList.get(i).getStyleID())!=null){
                    if(styles.getStyle(paragraphList.get(i).getStyleID()).getName().equals("heading 3")){
                        if(paragraphList.get(i).getText().length() > 9)
                            regresa.add(paragraphList.get(i).getText().toString().trim().substring(paragraphList.get(i).getText().length()-9)); 
                    }
                }
            }
        }
        return regresa;
    }
    
}
