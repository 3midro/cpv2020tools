package cpv2020tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;

public class ReadRequerimientos {
    public ReadRequerimientos() {
        super();
    }
    
    List<String> UO_Criterio = new LinkedList<>();
    List<String> unidadesObservacion = new LinkedList<>();

    public void test(String archivo) {
        try {
            FileInputStream fis = new FileInputStream(archivo);
            XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));
            List<XWPFParagraph> paragraphList = xdoc.getParagraphs();
            int i = 0;
            if (!(archivo.endsWith(".docx"))) {
                throw new FileFormatException();
            } else {

                for (XWPFParagraph paragraph : paragraphList) {

                    if (paragraph.getText().contains("Vectores")) {
                        System.out.println(paragraph.getText());
                        i++;
                        paragraph = paragraphList.get(i);
                        System.out.println(paragraph.getText());
                        continue;
                    }


                    if (paragraph.getText().contains("Funci�n de direccionamiento")) {
                        System.out.println(paragraph.getText());
                        i++;
                        paragraph = paragraphList.get(i);
                        System.out.println(paragraph.getText());
                        continue;
                    }
                    i++;
                }


                /*Iterator<XWPFTable> range=xdoc.getTablesIterator();
                                if(range.hasNext()){
                                    XWPFTable table = range.next();
                                List<XWPFTableRow> rowTable = table.getRows();
                                for(XWPFTableRow row : rowTable){
                                    List<XWPFTableCell> cellTable = row.getTableCells();
                                    for(XWPFTableCell cell : cellTable){
                                        if(cell!=null){
                                                System.out.println(cell.getText());
                                            }
                                    }}}*/

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void readTables(String archivo, String rutaDestino) {

        try {
            FileInputStream fis = new FileInputStream(archivo);
            XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));
            Iterator<IBodyElement> iter = xdoc.getBodyElementsIterator();
            List<XWPFParagraph> paragraphList = xdoc.getParagraphs();
            List<XWPFTable> tableList = xdoc.getTables();
            Boolean v = false, img = false, cond = false;
            String resultadosPosibles = "";
            ArrayList<Object> imagenes = new ArrayList<Object>();
            String imagenes_ = "";
            int limiteImagenes = 1;
            int numerodex = 0;
            int iteraciones = 0;
            String criterio = "";
            String[] condicion = { "", "" };
            String resultado = "";
            String UOespecifica= "";
            String UOgeneral= "";


            HashMap<String, Object> regreso = new HashMap<String, Object>();
            List<Object> datos = new ArrayList<Object>();


            while (iter.hasNext()) {
                IBodyElement elem = iter.next();
                if (elem instanceof XWPFParagraph) {
                    XWPFParagraph paragraph = (XWPFParagraph) elem;

                    if (paragraph.getStyle() != null && paragraph.getStyle().equals("Ttulo3")) {
                        if (paragraph.getParagraphText()
                                     .toString()
                                     .length() > 9) {
                            if (paragraph.getParagraphText().contains("T_CRESII06")) {
                                criterio = paragraph.getParagraphText()
                                                    .toString()
                                                    .substring(paragraph.getParagraphText().length() - 10)
                                                    .trim();
                            } else {
                                criterio = paragraph.getParagraphText()
                                                    .toString()
                                                    .substring(paragraph.getParagraphText().length() - 9)
                                                    .trim();
                            }

                            //System.out.println("CRITERIO: " + criterio);
                            datos.add(criterio);
                            datos.add("UO_" + criterio);
                        }
                    }

                    if (paragraph.getParagraphText()
                                 .toUpperCase()
                                 .contains("CONDICI�N:")) {
                        condicion = paragraph.getParagraphText()
                                             .toString()
                                             .trim()
                                             .split(":");
                        // datos.add("cond:"+condicion[1].trim());
                        cond = true;


                    } else if (paragraph.getParagraphText().contains("Vectores")) {
                        //System.out.println(paragraph.getParagraphText());
                        v = true;
                    } else if (paragraph.getParagraphText().contains("Funci�n de direccionamiento")) {
                        //System.out.println("resultados posibles: " + resultadosPosibles.substring(0, resultadosPosibles.length()-1));
                        /* System.out.println("resultados posibles: " + resultadosPosibles);
                        System.out.println("limite Imagenes: " + limiteImagenes);
                        System.out.println("Numero de X: " + numerodex);*/
                        datos.add(resultadosPosibles.substring(0, resultadosPosibles.length() - 1));
                        datos.add(limiteImagenes);
                        datos.add(numerodex);

                        limiteImagenes = 1;
                        resultadosPosibles = "";
                        numerodex = 0;
                        v = false;
                    } else if (paragraph.getParagraphText()
                               .contains("Im�genes, procedimientos y n�mero de tratamiento")) {
                        //System.out.println(paragraph.getParagraphText());
                        img = true;
                    } else if (paragraph.getParagraphText()
                               .contains("Im�genes para la combinaci�n de variables y procedimientos")) {
                        if (datos.size() > 3) {
                            iteraciones++;
                            if (!cond) {
                                datos.add("");
                            } else {
                                datos.add(condicion[1].trim());
                            }
                            condicion[0] = "";
                            condicion[1] = "";
                            cond = false;
                            //datos.add(criterio);
                            //regreso.put(criterio, datos);

                            /*System.out.println(">>>>INICIO CRITERIO "+ criterio +"<<<<<");
                                for (Object d : datos) {
                                    System.out.println(d);
                                }

                                //System.out.println(datos);
                                System.out.println("--------FIN CRITERIO " + criterio + "-----------------------------");
                                */
                            resultado = crearClasesCriterios(datos);
                            ClassCreator(resultado, datos, rutaDestino);
                            datos.clear();
                        }

                    }
                } else if (elem instanceof XWPFTable && v == true) {
                    XWPFTable table = (XWPFTable) elem;
                    //System.out.println(table.getText());
                    resultadosPosibles += table.getNumberOfRows() + ",";
                    limiteImagenes = limiteImagenes * table.getNumberOfRows();
                    numerodex++;
                    String[] palabras = table.getText().split(" ");
                    String regex = "^[A-Z_]{3,}$|^[A-Z_]{3,}[\\d]{1,}$";
                    Pattern patron = Pattern.compile(regex);

                    for (String palabra : palabras) {
                        Matcher emparejador = patron.matcher(palabra);
                        boolean esCoincidente = emparejador.find();

                        if (esCoincidente) {
                            System.out.println("variable reconocida del criterio "+ criterio + ": " + palabra);
                            if(!UO_Criterio.contains(palabra.trim())) UO_Criterio.add(palabra.trim());
                            if(!unidadesObservacion.contains(palabra.trim())) unidadesObservacion.add(palabra.trim());
                        }
                    }


                } else if (elem instanceof XWPFTable && img == true) {

                    XWPFTable table = (XWPFTable) elem;
                    List<XWPFTableRow> rowTable = table.getRows();
                    int r = 0;
                    for (XWPFTableRow row : rowTable) {
                        int c = 0;
                        List<XWPFTableCell> cellTable = row.getTableCells();
                        for (XWPFTableCell cell : cellTable) {
                            if (cell != null && r > 0 && c == 0) {
                                //System.out.println(cell.getText());
                                imagenes.add(cell.getText().replaceAll(" ", ""));
                                imagenes_ += cell.getText()
                                                 .replaceAll(" ", "")
                                                 .concat("|");
                            }
                            c++;
                        }
                        r++;
                    }
                    img = false;
                    //System.out.println("imagenes:");
                    //datos.add("imagenes del criterio " + criterio);
                    datos.add(imagenes_.substring(0, imagenes_.length() - 1));
                    for (Object im : imagenes) {
                        //System.out.println(im);


                    }
                    imagenes.clear();
                    imagenes_ = "";
                }
            }

            System.out.println(iteraciones + " Clases generadas");

            //System.out.println(regreso);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String crearClasesCriterios(List<Object> datos) throws IOException {
        String resultado = "";
        String imagenes = datos.get(5).toString();
        String imgCom = "";
        String cases = "";
        String equis = "";
        Calendar fecha = new GregorianCalendar();
        int a�o = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);

        for (Integer i = 1; i <= (Integer) datos.get(4); i++) {
            equis = equis.concat("x" + i.toString().concat(","));
        }
        equis = equis.substring(0, equis.length() - 1);

        String[] imagenes_ = imagenes.split("\\|");
        for (int j = 0; j < imagenes_.length; j++) {
            imgCom += "\"" + imagenes_[j] + "\"" + ",";
            cases += "                case "+imagenes_[j].replaceAll(",", ": case ")+":\n\t\t\t\tbreak;\n"; 
        }
        imgCom = imgCom.substring(0, imgCom.length() - 1);
        resultado =
            "    import inegi.org.mx.modulos.persona.beans.uobservacion.UO_PERSONA;\n" + 
            "    import utilerias.arrays.Encuentra;\n" +
            "    import inegi.org.mx.validacion.criterios.claseabstracta.Criterio;\n\n" +
            "    import static utilerias.numeros.Numero.nvl;\n" +
            "    import org.apache.log4j.Logger;\n\n" + "    public class " + datos.get(0) + " extends Criterio{\n" +
            "    private Logger log = Logger.getLogger(this.getClass().getName());\n" + "    private UO_PERSONA" +
            " dato;\n" + "    //numero de elementos que indique el requerimiento (x1,x2,x3....)\n" +
            "    private int " + equis + ";\n" + "    //numero de imagenes generadas\n" +
            "    private final int limiteImagenes = " + datos.get(3) + ";\n" +
            "    //resultados posibles de cada una de las x�s\n" +
            "    private final int[] resultadosPosibles = new int[] { " + datos.get(2) + " };\n" +
            "    //valores tomados de la tabla resumen de imagenes\n" +
            "    private final String[] imagenes = new String[] { " + imgCom + " };" + "    //no cambia\n" +
            "    public " + datos.get(0) + "(UO_PERSONA dato) throws Exception {\n" + "        this();\n" +
            "        this.dato = dato;\n" + "    }\n" + "\n" + "    //fecha de creacion del criterio\n" +
            "    public " + datos.get(0)+"() {\n" + "        //fecha en formato a�o mes dia\n" +
            "        setVersion(1.0f,\"" + a�o + (mes + 1) + dia + "\");\n" + "    }\n" + "\n" + "    /**\n" +
            "     * condicion de entrada del requerimiento\n" + "     * @return\n" + "     */\n" +
            "    public boolean condicion() {\n" + "        // " + datos.get(6) + "\n" + "        return true;\n" +
            "    }\n" + "           \n" + "    public void validar() throws Exception {\n" + "        try {\n" +
            "            resolver(dato.getLlave(),resultadosPosibles,imagenes,limiteImagenes,new int[] { " + equis +
            " });\n" + "\n" + "            log.debug(\"Diagnostico \" + getDiagnostico());\n" + "\n" +
            "            //cases de acuerdo a los tratamientos de la tabla resumen de imagenes\n" +
            "            String valor = null;\n" + "            switch (getDiagnostico()) {\n" +
            cases+"               default:\n" +
            "                throw new Exception(\"Diagnostico no encontrado \" + getDiagnostico());\n" + "            }\n" + "\n" +
            "        } catch (Exception e) {\n" + "            log.error(e.getMessage());\n" +
            "            //e.printStackTrace();\n" +
            "            throw new Exception(\"[\" + dato.getID_PER() + \"] \" + this.getNombreCriterio() + \" \" + e.getMessage());\n" +
           "        } finally {\n" + "\n" + "        }\n" + "    }\n } ";

        return resultado;
    }

    public void ClassCreator(String resultado, List<Object> datos, String rutaDestino) throws IOException {
        String ruta = rutaDestino + "\\" + datos.get(0) + ".java";
        File archivo = new File(ruta);
        //si no existe el archivo, es creado
        if (!archivo.exists()) {
            archivo.createNewFile();
        }
        FileWriter fw = new FileWriter(archivo);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(resultado);
        bw.close();
        System.out.println("Clase " + datos.get(0) + " generada");
    }
    
    public void UOespecificaCreator(String UOespecifica, List<Object> datos, String rutaDestino) throws IOException {
        String ruta = rutaDestino + "\\" + datos.get(1) + ".java";
        File archivo = new File(ruta);
        //si no existe el archivo, es creado
        if (!archivo.exists()) {
            archivo.createNewFile();
        }
        FileWriter fw = new FileWriter(archivo);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(UOespecifica);
        bw.close();
        System.out.println("Clase " + datos.get(1) + " generada");
    }
    
    public String UOCreator(List<Object> datos, List<String> UO){
            String aux = "";
            String vars = "";
            String getnset = "";
            String nomArchivo = "";
            
            
            for (String str : UO) {
                if(UO.size() >50){
                        nomArchivo = "UOgeneral";
                }else{
                        nomArchivo = datos.get(1).toString();
                    }
                    vars += "       private int " + str.toString().trim() + ";\r\n";
                    getnset +=
                        "   public void set" + str.toString().trim() + "(int "  + str.toString().trim() + "){\r\n" +
                        "           this." + str.toString().trim() + " = " + str.toString().trim() + ";\r\n" + "  }\r\n\r\n" +
                        "   @JsonProperty(\"" + str.toString().trim() + "\")\r\n" + "  public int get" +
                        str.toString().trim() + "(){\r\n" + "          return " + str.toString().trim() + ";\r\n" +
                        "   }\r\n\r\n";
                }        
            //System.out.println(vars);
            aux =             
            "import com.fasterxml.jackson.annotation.JsonIgnore;\n" + 
            "import com.fasterxml.jackson.annotation.JsonIgnoreProperties;\n" + 
            "import com.fasterxml.jackson.annotation.JsonProperty;\n" + 
            "\n" + 
            "import java.io.Serializable;\n" + 
            "\n" + 
            "import java.sql.Connection;\n" + 
            "import java.sql.SQLException;\n" + 
            "\n"+
            "@JsonIgnoreProperties(ignoreUnknown = true)\r\n" + "public class " + nomArchivo +
            " extends Bean implements Cloneable, Serializable {\r\n" +
            "    private static final long serialVersionUID = 1L;\r\n\r\n";
                
            aux += vars + "\n    private List<Traza> traza = new ArrayList<>();\n" + 
        "    private String ERROR_AL_PROCESAR;\n\n" +
        "    public "+nomArchivo+"() {\n" +
        "        super();\n" +
        "    }\n\n" +
        "    @JsonProperty(\"LLAVE\")\n" + 
        "    @Override\n" + 
        "    public Object[] getLlave() {\n" + 
        "        return new Object[] { getID_UE() };\n" + 
        "    }\n" + 
        "\n" + 
        "    @Override\n" + 
        "    public Object[] toArray() {\n" + 
        "        return new Object[0];\n" + 
        "    }\n" + 
        "\n" + 
        "    @Override\n" + 
        "    public void actualizaDesc(Connection conn) throws SQLException {\n" + 
        "\n" + 
        "    }\n" + 
        "\n" + 
        "    @JsonProperty(\"TRAZA\")\n" + 
        "    public Traza[] getTraza() {\n" + 
        "        return traza.toArray(new Traza[traza.size()]);\n" + 
        "    }\n" + 
        "\n" + 
        "    @JsonIgnore\n" + 
        "    public String getTrazaToString() {\n" + 
        "        return traza.toString().replace(\"}, {\", \"},{\");\n" + 
        "    }\n" + 
        "\n" + 
        "    public void addTraza(Traza traza) {\n" + 
        "        if (null != traza) {\n" + 
        "            this.traza.add(traza);\n" + 
        "        }\n" + 
        "    }\n" + 
        "\n" + 
        "    public void setTraza(List<Traza> traza) {\n" + 
        "        this.traza = traza;\n" + 
        "    }\n" + 
        "\n" + 
        "    public void setERROR_AL_PROCESAR(String ERROR_AL_PROCESAR) {\n" + 
        "        this.ERROR_AL_PROCESAR = ERROR_AL_PROCESAR;\n" + 
        "    }\n" + 
        "\n" + 
        "    @JsonProperty(\"ERROR_AL_PROCESAR\")\n" + 
        "    public String getERROR_AL_PROCESAR() {\n" + 
        "        return ERROR_AL_PROCESAR;\n" + 
        "    }\n\n" + getnset + "\r\n\r\n";
            aux += "}";
            return aux;
        }
    
    public void UOgeneralCreator(String UOgeneral, String rutaDestino) throws IOException {
        String ruta = rutaDestino + "\\UO_general.java";
        File archivo = new File(ruta);
        //si no existe el archivo, es creado
        if (!archivo.exists()) {
            archivo.createNewFile();
        }
        FileWriter fw = new FileWriter(archivo);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(UOgeneral);
        bw.close();
        System.out.println("Clase UO_GENERAL generada");
    }
}

