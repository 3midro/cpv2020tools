package cpv2020tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

public class ReadRequerimientosVivienda {
    public ReadRequerimientosVivienda() {
        super();
    }
    
    
    public void readTables(String archivo, String rutaDestino) {

        try {
            FileInputStream fis = new FileInputStream(archivo);
            XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));
            Iterator<IBodyElement> iter = xdoc.getBodyElementsIterator();
            List<XWPFParagraph> paragraphList = xdoc.getParagraphs();
            List<XWPFTable> tableList = xdoc.getTables();
            Boolean v = false, img = false, cond =false;
            String resultadosPosibles = "";
            ArrayList<Object> imagenes = new ArrayList<Object>();
            String imagenes_ = "";
            int limiteImagenes = 1;
            int numerodex = 0;
            int iteraciones = 0;
            String criterio = "";
            String[] condicion = { "", "" };
            String resultado="";
            


            HashMap<String, Object> regreso = new HashMap<String, Object>();
            List<Object> datos = new ArrayList<Object>();


            while (iter.hasNext()) {
                IBodyElement elem = iter.next();
                if (elem instanceof XWPFParagraph) {
                    XWPFParagraph paragraph = (XWPFParagraph) elem;

                    if (paragraph.getStyle() != null && (paragraph.getStyle().equals("Ttulo3")|| paragraph.getStyle().equals("Ttulo2")) && paragraph.getParagraphText().contains("T_") ) {      
                        if (paragraph.getParagraphText()
                                     .toString()
                                     .length() > 9) {
                            criterio = paragraph.getParagraphText().toString().substring(paragraph.getParagraphText().length() - 9).trim();
                            
                            //System.out.println("CRITERIO: " + criterio);
                            datos.add(criterio);
                            datos.add("UO_"+criterio);
                        }
                    }

                    if (paragraph.getParagraphText()
                                 .toUpperCase()
                                 .contains("CONDICI�N:")) {
                        condicion = paragraph.getParagraphText()
                                             .toString()
                                             .trim()
                                             .split(":");
                       // datos.add("cond:"+condicion[1].trim());
                        cond = true;


                    } else if (paragraph.getParagraphText().contains("Vector")) {
                        //System.out.println(paragraph.getParagraphText());
                        v = true;
                    } else if (paragraph.getParagraphText().contains("Funci�n de direccionamiento")) {
                        //System.out.println("resultados posibles: " + resultadosPosibles.substring(0, resultadosPosibles.length()-1));
                        /* System.out.println("resultados posibles: " + resultadosPosibles);
                        System.out.println("limite Imagenes: " + limiteImagenes);
                        System.out.println("Numero de X: " + numerodex);*/
                        datos.add(resultadosPosibles.substring(0, resultadosPosibles.length()-1));
                        datos.add(limiteImagenes);
                        datos.add(numerodex);

                        limiteImagenes = 1;
                        resultadosPosibles = "";
                        numerodex = 0;
                        v = false;
                    } else if (paragraph.getParagraphText()
                               .contains("Im�genes, procedimientos y n�mero de tratamiento")) {
                        //System.out.println(paragraph.getParagraphText());
                        img = true;
                    } else if (paragraph.getParagraphText()
                               .contains("Im�genes para las combinaciones de variables y procedimientos")){
                            if (datos.size()>3) {
                                iteraciones ++;
                                if (!cond){
                                        datos.add("");
                                }else{
                                        datos.add(condicion[1].trim());
                                    }
                                condicion[0] = ""; condicion[1] = "";
                                cond = false;
                                //datos.add(criterio);
                                //regreso.put(criterio, datos);
                                
                               /* System.out.println(">>>>INICIO CRITERIO "+ criterio +"<<<<<");
                                for (Object d : datos) {
                                    System.out.println(d);
                                }
                                
                                //System.out.println(datos);
                                System.out.println("--------FIN CRITERIO " + criterio + "-----------------------------");
                                */
                                resultado = crearClasesCriterios(datos);
                                ClassCreator(resultado,datos, rutaDestino);
                                datos.clear();
                                }
                        
                        }
                } else if (elem instanceof XWPFTable && v == true) {
                    XWPFTable table = (XWPFTable) elem;
                    //System.out.println(table.getText());
                    resultadosPosibles += table.getNumberOfRows() + ",";
                    limiteImagenes = limiteImagenes * table.getNumberOfRows();
                    numerodex++;

                } else if (elem instanceof XWPFTable && img == true) {

                    XWPFTable table = (XWPFTable) elem;
                    List<XWPFTableRow> rowTable = table.getRows();
                    int r = 0;
                    for (XWPFTableRow row : rowTable) {
                        int c = 0;
                        List<XWPFTableCell> cellTable = row.getTableCells();
                        for (XWPFTableCell cell : cellTable) {
                            if (cell != null && r > 0 && c == 0) {
                                //System.out.println(cell.getText());
                                imagenes.add(cell.getText().replaceAll(" ", ""));
                                imagenes_ += cell.getText()
                                                 .replaceAll(" ", "")
                                                 .concat("|");
                            }
                            c++;
                        }
                        r++;
                    }
                    img = false;
                    //System.out.println("imagenes:");
                    //datos.add("imagenes del criterio " + criterio);
                    datos.add(imagenes_.substring(0, imagenes_.length()-1));
                    for (Object im : imagenes) {
                        //System.out.println(im);

                    }
                    imagenes.clear();
                    imagenes_ = "";
                }
            }
            System.out.println(iteraciones + " Clases generadas");

            //System.out.println(regreso);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    
    public String crearClasesCriterios(List<Object> datos) throws IOException {
            String resultado = "";
            String imagenes = datos.get(5).toString();
            String imgCom = "";
            String cases = "";
            String equis="";
            Calendar fecha = new GregorianCalendar();
            int a�o = fecha.get(Calendar.YEAR);
            int mes = fecha.get(Calendar.MONTH);
            int dia = fecha.get(Calendar.DAY_OF_MONTH);
            
            for(Integer i=1; i<=(Integer)datos.get(4); i++){
                                equis = equis.concat("x"+i.toString().concat(","));
                            }
                        equis = equis.substring(0, equis.length()-1);
                        
                        String[] imagenes_ = imagenes.split("\\|");
                        for(int j=0; j<imagenes_.length; j++){
                                imgCom += "\""+imagenes_[j]+"\""+",";
                            cases += "                case "+imagenes_[j].replaceAll(",", ": case ")+":\n\t\t\t\tbreak;\n"; 
                            }
                        imgCom = imgCom.substring(0,imgCom.length()-1);
                        resultado = 
                        "    import inegi.org.mx.modulos.vivienda.beans.uobservacion.UO_VIVIENDA;\n" + 
                        "    import utilerias.arrays.Encuentra;\n" +
                        "    import inegi.org.mx.validacion.criterios.claseabstracta.Criterio;\n\n" +
                        "    import static utilerias.numeros.Numero.nvl;\n" +
                        "    import org.apache.log4j.Logger;\n\n" +
                        "    public class "+datos.get(0)+" extends Criterio{\n" + 
                        "    private Logger log = Logger.getLogger(this.getClass().getName());\n" +  
                        "    private UO_VIVIENDA dato;\n" +
                        "    //numero de elementos que indique el requerimiento (x1,x2,x3....)\n" + 
                        "    private int " +equis+ ";\n" + 
                        "    //numero de imagenes generadas\n" + 
                        "    private final int limiteImagenes = "+datos.get(3)+";\n" + 
                        "    //resultados posibles de cada una de las x�s\n" + 
                        "    private final int[] resultadosPosibles = new int[] { "+datos.get(2)+" };\n" + 
                        "    //valores tomados de la tabla resumen de imagenes\n" + 
                        "    private final String[] imagenes = new String[] { "+imgCom+" };" +
                        "    //no cambia\n" + 
                        "    public "+datos.get(0)+ "(UO_VIVIENDA dato) throws Exception {\n" + 
                        "        this();\n" + 
                        "        this.dato = dato;\n" + 
                        "    }\n" + 
                        "\n" + 
                        "    //fecha de creacion del criterio\n" + 
                        "    public "+datos.get(0)+"() {\n" + 
                        "        //fecha en formato a�o mes dia\n" + 
                        "        setVersion(1.0f,\""+a�o+(mes+1)+dia+"\");\n" + 
                        "    }\n" + 
                        "\n" + 
                        "    /**\n" + 
                        "     * condicion de entrada del requerimiento\n" + 
                        "     * @return\n" + 
                        "     */\n" + 
                        "    public boolean condicion() {\n" +
                        "        // "+datos.get(6)+"\n"+
                        "        return true;\n" + 
                        "    }\n" + 
                        "           \n" + 
                        "    public void validar() throws Exception {\n" + 
                        "        try {\n" + 
                        "            resolver(dato.getLlave(),resultadosPosibles,imagenes,limiteImagenes,new int[] { "+equis+" });\n" + 
                        "\n" + 
                        "            log.debug(\"Diagnostico \" + getDiagnostico());\n" + 
                        "\n" + 
                        "            //cases de acuerdo a los tratamientos de la tabla resumen de imagenes\n" + 
                        "            String valor = null;\n" + 
                        "            switch (getDiagnostico()) {\n" + cases + 
                        "            default:\n" + 
                        "                throw new Exception(\"Diagnostico no encontrado \" + getDiagnostico());\n" + 
                        "            }\n" + 
                        "\n" + 
                        "        } catch (Exception e) {\n" + 
                        "            log.error(e.getMessage());\n" + 
                        "            //e.printStackTrace();\n" +             
                        "            throw new Exception(\"[\" + dato.getID_INM() + \"] \" + this.getNombreCriterio() + \" \" + e.getMessage());\n" +
                        "        } finally {\n" + 
                        "\n" + 
                        "        }\n" + 
                        "    } \n }";
                        
                        return resultado;
        }
    
    public void ClassCreator(String resultado, List<Object> datos, String rutaDestino) throws IOException {
            String ruta = rutaDestino + "\\" + datos.get(0) + ".java";
            File archivo = new File(ruta);
            //si no existe el archivo, es creado
            if(!archivo.exists()){
                    archivo.createNewFile();
                }
            FileWriter fw = new FileWriter(archivo);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(resultado);
            bw.close();
            System.out.println("Clase " + datos.get(0) + " generada");
        } 
    
    
}
