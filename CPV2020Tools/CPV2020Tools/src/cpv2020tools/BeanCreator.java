package cpv2020tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class BeanCreator {

    public void beginProceso(String rutaOrigen, String rutaDestino) {
        try {
            rutaOrigen = rutaOrigen.replace("\\", "\\\\");
            rutaDestino = rutaDestino.replace("\\", "\\\\");
            final File carpeta = new File(rutaOrigen);
            listarFicherosFD(carpeta, rutaDestino);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void listarFicherosFD(final File carpeta, String rutaDestino) {
        for (final File ficheroEntrada : carpeta.listFiles()) {
            if (!ficheroEntrada.isDirectory()) {
                crearBean(ficheroEntrada, rutaDestino);
            }
        }
    }

    public void crearBean(File archivo, String rutaDestino) {
        String contenido = readXLSX(archivo);
        try {
            String nomArch = archivo.getName();
            //nomArch = nomArch.substring(((nomArch.length() - 8) * 1), (nomArch.length() - 5));
            nomArch = nomArch.substring(0,nomArch.length()-5);
            String ruta = rutaDestino + "\\" + nomArch + ".java";
            File file = new File(ruta);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(contenido);
            bw.close();
            System.out.println("Clase " + nomArch + " generada");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String readXLSX(File excelFile) {
        List<String[]> arrayDatos = new ArrayList<>();
        String resultado = "";
        String vars = "";
        String getnset = "";
        try {
            InputStream excelStream = null;
            excelStream = new FileInputStream(excelFile);
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(excelStream);
            XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
            XSSFRow hssfRow = xssfSheet.getRow(xssfSheet.getTopRow());
            for (Row row : xssfSheet) {
                String[] datos = new String[hssfRow.getLastCellNum()];
                if (row.getRowNum() != 0) {
                    for (Cell cell : row) {
                        datos[cell.getColumnIndex()] =
                            (cell.getCellType() == Cell.CELL_TYPE_STRING) ? cell.getStringCellValue() :
                            (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) ? "" + cell.getNumericCellValue() :
                            (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) ? "" + cell.getBooleanCellValue() :
                            (cell.getCellType() == Cell.CELL_TYPE_BLANK) ? "BLANK" :
                            (cell.getCellType() == Cell.CELL_TYPE_FORMULA) ? "FORMULA" :
                            (cell.getCellType() == Cell.CELL_TYPE_ERROR) ? "ERROR" : "";

                    }

                    arrayDatos.add(datos);
                    datos = new String[hssfRow.getLastCellNum()];
                }

            }
            excelStream.close();
            for (String[] str : arrayDatos) {
                //obtiene el tipo
                str[5] = (str[5] != null && str[5].length() > 0) ? str[5].toString() : "";
                str[6] = (str[6] != null && str[6].length() > 0) ? str[6].toString() : "";
                str[7] = (str[7] != null && str[7].length() > 0) ? str[7].toString() : "";
                String tipo = getTipo(str[5].toString(), str[6].toString(), str[7].toString());
                vars += "	private " + tipo + " " + str[2].toString().replace(" ", "") + ";\r\n";
                getnset +=
                    "	public void set" + str[2].toString().replace(" ", "") + "(" + tipo + " " + str[2].toString().replace(" ", "") + "){\r\n" +
                    "		this." + str[2].toString().replace(" ", "") + " = " + str[2].toString().replace(" ", "") + ";\r\n" + "	}\r\n\r\n" +
                    "	@JsonProperty(\"" + str[2].toString().replace(" ", "") + "\")\r\n" + "	public " + tipo + " get" +
                    str[2].toString().replace(" ", "") + "(){\r\n" + "		return " + str[2].toString().replace(" ", "") + ";\r\n" +
                    "	}\r\n\r\n";
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        String nomArch = excelFile.getName();
        //nomArch = nomArch.substring(((nomArch.length() - 8) * 1), (nomArch.length() - 5));
        nomArch = nomArch.substring(0,nomArch.length()-5);
        resultado =
            "import com.fasterxml.jackson.annotation.JsonIgnore;\n" + 
            "import com.fasterxml.jackson.annotation.JsonIgnoreProperties;\n" + 
            "import com.fasterxml.jackson.annotation.JsonProperty;\n" + 
            "\n" + 
            "import inegi.org.mx.modulos.dao.Bean;\n" + 
            "import inegi.org.mx.validacion.traza.Traza;\n" + 
            "\n" + 
            "import java.io.Serializable;\n" + 
            "\n" + 
            "import java.sql.Connection;\n" + 
            "import java.sql.SQLException;\n" + 
            "\n" + 
            "import java.util.ArrayList;\n" + 
            "import java.util.List;\n\n" +
            "@JsonIgnoreProperties(ignoreUnknown = true)\r\n" + "public class " + nomArch +
            " extends Bean implements Cloneable, Serializable {\r\n" +
            "    private static final long serialVersionUID = 1L;\r\n\r\n";
        resultado += vars + "\n    private List<Traza> traza = new ArrayList<>();\n" + 
        "    private String ERROR_AL_PROCESAR;\n\n" +
        "    public "+nomArch+"() {\n" +
        "        super();\n" +
        "    }\n\n" +
        "    @JsonProperty(\"LLAVE\")\n" + 
        "    @Override\n" + 
        "    public Object[] getLlave() {\n" + 
        "        return new Object[] { getID_UE() };\n" + 
        "    }\n" + 
        "\n" + 
        "    @Override\n" + 
        "    public Object[] toArray() {\n" + 
        "        return new Object[0];\n" + 
        "    }\n" + 
        "\n" + 
        "    @Override\n" + 
        "    public void actualizaDesc(Connection conn) throws SQLException {\n" + 
        "\n" + 
        "    }\n" + 
        "\n" + 
        "    @JsonProperty(\"TRAZA\")\n" + 
        "    public Traza[] getTraza() {\n" + 
        "        return traza.toArray(new Traza[traza.size()]);\n" + 
        "    }\n" + 
        "\n" + 
        "    @JsonIgnore\n" + 
        "    public String getTrazaToString() {\n" + 
        "        return traza.toString().replace(\"}, {\", \"},{\");\n" + 
        "    }\n" + 
        "\n" + 
        "    public void addTraza(Traza traza) {\n" + 
        "        if (null != traza) {\n" + 
        "            this.traza.add(traza);\n" + 
        "        }\n" + 
        "    }\n" + 
        "\n" + 
        "    public void setTraza(List<Traza> traza) {\n" + 
        "        this.traza = traza;\n" + 
        "    }\n" + 
        "\n" + 
        "    public void setERROR_AL_PROCESAR(String ERROR_AL_PROCESAR) {\n" + 
        "        this.ERROR_AL_PROCESAR = ERROR_AL_PROCESAR;\n" + 
        "    }\n" + 
        "\n" + 
        "    @JsonProperty(\"ERROR_AL_PROCESAR\")\n" + 
        "    public String getERROR_AL_PROCESAR() {\n" + 
        "        return ERROR_AL_PROCESAR;\n" + 
        "    }\n\n" + getnset + "\r\n\r\n";
        resultado += "}";
        return resultado;
    }

    public static String getTipo(String tipo, String longitud, String v_data_precision) {
        tipo = tipo.toUpperCase();
        float v_data_precision_ = 0;
        String tipo_java = "";
        if (tipo.contentEquals("")) {
            tipo_java = "Long";
        } else if (tipo.equals("NUMBER") || tipo.equals("NUM�RICO")) {
            v_data_precision_ = Float.parseFloat(v_data_precision);
            tipo_java = (v_data_precision_ >= 11) ? "Long" : "Integer";
        } else if (tipo.equals("VARCHAR2") || tipo.equals("CHAR") || tipo.equals("DATE") || tipo.equals("CARACTER") || tipo.equals("FECHA")) {
            tipo_java = "String";
        }
        return tipo_java;
    }
}
