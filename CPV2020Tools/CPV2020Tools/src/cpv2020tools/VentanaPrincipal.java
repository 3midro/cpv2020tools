package cpv2020tools;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.lang.reflect.InvocationTargetException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities; 

public class VentanaPrincipal extends JFrame implements ActionListener {
    
    JPanel panel = new JPanel();
    int altura = 450, anchura = 720;
    public VentanaPrincipal() {
        super();
        configurarVentana();
    }

    public static void main(String[] args) {
        VentanaPrincipal ventanaPrincipal = new VentanaPrincipal();
        ventanaPrincipal.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        // TODO Implement this method
    }
    
    private void configurarVentana(){
        this.setTitle("Censo de Población y Vivienda 2020 - INEGI - Tools");
        this.setSize(anchura,altura);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setMinimumSize(new Dimension(anchura,altura));
        this.setMaximumSize(new Dimension(anchura,altura));
        this.getContentPane().setBackground(Color.WHITE);
        this.setLocationRelativeTo(null);
        colocarPaneles();
    }
    
    private void colocarPaneles(){
        panel.setBackground(Color.WHITE);
        panel.setSize(720, 405);
        panel.setLayout(new BorderLayout());
        colocarINEGIBackground();
        colocarBotones();
        this.getContentPane().add(panel);
        
    }
    
    private void colocarINEGIBackground(){
        ImageIcon imagen = new ImageIcon(getClass().getResource("../logo.jpg"));
        JLabel label = new JLabel();
        label.setSize(480, 145);
        label.setIcon(new ImageIcon(imagen.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT)));
        label.setBounds(120,0,480,145);
        this.getContentPane().add(label);
    }
    
    
    private void colocarBotones(){
        final JTextField fieldElegir = new JTextField();
        final JTextField fieldDestino = new JTextField();
        JButton buttonElegir = new JButton("...");
        buttonElegir.setBounds(570, 160, 30, 30);
        JButton buttonProcesar = new JButton("...");
        buttonProcesar.setBounds(570, 220, 30, 30);
        JLabel labelRutaOrigen = new JLabel("Ruta de origen seleccionada", SwingConstants.LEFT);
        labelRutaOrigen.setBounds(25, 160, 295, 30);
        JLabel labelRutaDestino = new JLabel("Ruta destino seleccionada", SwingConstants.LEFT);
        labelRutaDestino.setBounds(25, 220, 280, 30);
        JLabel labelGenerador = new JLabel("Generador: ", SwingConstants.CENTER);
        labelGenerador.setBounds(15, 280, 210, 30);
        ImageIcon imagen = new ImageIcon(getClass().getResource("../excel2.png"));
        JButton doExcel = new JButton("");
        doExcel.setSize(75, 75);
        doExcel.setIcon(new ImageIcon(imagen.getImage().getScaledInstance(doExcel.getWidth(), doExcel.getHeight(), Image.SCALE_DEFAULT)));
        doExcel.setBounds(300, 320, 50, 50);
        this.getContentPane().add(doExcel);
        ImageIcon imagen2 = new ImageIcon(getClass().getResource("../java2.png"));
        JButton doJava = new JButton("");
        doJava.setSize(60, 60);
        doJava.setIcon(new ImageIcon(imagen2.getImage().getScaledInstance(doJava.getWidth(), doJava.getHeight(), Image.SCALE_DEFAULT)));
        doJava.setBounds(400, 320, 50, 50);
        final JProgressBar progress = new JProgressBar();
        progress.setBounds(200, 385, 320, 30);
        this.getContentPane().add(progress);
        this.getContentPane().add(doJava);
        
        ImageIcon imagen3 = new ImageIcon(getClass().getResource("../java2.png"));
        JButton doCriterio = new JButton("");
        doCriterio.setSize(75, 75);
        doCriterio.setIcon(new ImageIcon(imagen3.getImage().getScaledInstance(doCriterio.getWidth(), doCriterio.getHeight(), Image.SCALE_DEFAULT)));
        doCriterio.setBounds(500, 320, 50, 50);
        this.getContentPane().add(doCriterio);
        
        
        this.getContentPane().add(buttonElegir);
        this.getContentPane().add(buttonProcesar);
        this.getContentPane().add(labelRutaOrigen);
        this.getContentPane().add(labelRutaDestino);
        this.getContentPane().add(labelGenerador);
        this.getContentPane().add(fieldElegir);
        this.getContentPane().add(fieldDestino);
        buttonElegir.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                fieldElegir.setBounds(260, 160, 300, 30);
                EventosBotones eventosBotones = new EventosBotones();
                fieldElegir.setText(eventosBotones.launchFileSearcher(VentanaPrincipal.this));
            }
        });
        buttonProcesar.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                fieldDestino.setBounds(260,220,300,30);
                EventosBotones eventosBotones = new EventosBotones();
                fieldDestino.setText(eventosBotones.launchFileSearcher(VentanaPrincipal.this));
            }
        });
        doExcel.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                EventosBotones eventosBotones = new EventosBotones();
                eventosBotones.beginProceso(fieldElegir.getText().toString(), fieldDestino.getText().toString());
            }
        });
        doJava.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BeanCreator beanCreator = new BeanCreator();
                beanCreator.beginProceso(fieldElegir.getText().toString(), fieldDestino.getText().toString());
                JOptionPane.showMessageDialog(null, "Se generaron correctamente los archivos");
            }
        });
    }
}
