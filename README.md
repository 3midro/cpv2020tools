# CPV2020Tools

El objetivo de este repositorio es alojar el proyecto __Tools__ que funcionará como __Herramientas__ necesarias durante el desarrollo del proyecto *Censo de Población y Vivienda*.


## Librerías necesarias

Requeriremos de las siguientes librerías __(JAR)__
*	ooxml-schemas-1.1
*	poi-3.11
*	poi-ooxml-3.11
*	xmlbeans-2.3.0

## Instrucciones

Realizar los siguientes pasos en el orden mostrado:
* _Clonar_ el proyecto.
* __Importar__ el proyecto con _jDeveloper_
* __Importar__ las __librerías necesarias mencionadas previamente__ al proyecto, dando clic en _Tools_ -> _Manage Libraries_
	* Dar clic en __New__ e __importar una a una__ todas las librerías __(4)__
	* Dar clic en __Ok__
	* Dar __Clic derecho__ en _Proyecto_ -> _Project Properties_ -> _Libraries and Classpath_
	* __Agregar__ las __librerias__ dando clic en __Add Library..__ y __seleccionando__ las registradas en la carpeta __User__
* __Abrir__ la clase _VentanaPrincipal.java_ y dar __Clic derecho__ -> __Run__

## Anexos

![img1](/img/img1.png)

![img2](/img/img2.png)

![img3](/img/img3.png)

![img4](/img/img4.png)

![img5](/img/img5.png)

![img6](/img/img6.png)

![img7](/img/img7.png)

![img8](/img/img8.png)